
const todos = {
    list: (ctx) => {
        ctx.body = [{
            text: 'Buy tacos',
            isDone: true,
        }, {
            text: 'Eat tacos',
            isDone: false,
        }]
    }
}