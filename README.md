# A very very basic Siren API + Polymer implementation

## Fair Warning
I'm brand new to Siren and Polymer 2. This repo is a terrible implementation just to get use to the schema and how HATEOAS works in combination with Polymer.

## Installation Instructions
1. `yarn` or `npm install`
2. `yarn start` or `npm start`

## Interesting bits
Inside `index.html` you'll see `<todo-list href="..." token="admin">`. Instead of a static and highly secure plaintext string you could use your JWT token and pass that through to your component. The API uses the token to decide what actions (and in the future, links) to send back to you. Changing this to 'user' will reduce your abilities. In theory this should allow you to write re-usable components that all just take a uri and a token and can figure out what to display to the user. Due to HATEOAS, everything should be cached and due to HTTP2, all those calls shouldn't be horrible.
