const _ = require('koa-route')
const Koa = require('koa')
const serve = require('koa-static')
const app = new Koa()
const siren = require('siren-writer')('http://localhost:3000/')

app.use(serve(`${__dirname}/bower_components/`))
app.use(serve(`${__dirname}/public/`))

const db = new Map()
db.set('0', { id: '0', text: 'Buy tacos', isDone: true })
db.set('1', { id: '1', text: 'Eat tacos', isDone: false })

app.use(_.get('/todos', async ctx => {
    const todos = siren({
        class: ['todo-list'],
        properties: {
            total: db.size,
        },
        entities: [{
            class: ['todo-item'],
            rel: 'http://localhost:3000/rels/todo-item',
            href: 'http://localhost:3000/todos/0',
        }, {
            class: ['todo-item'],
            rel: 'http://localhost:3000/rels/todo-item',
            href: 'http://localhost:3000/todos/1',
        }]
    })
    ctx.body = todos
}))

app.use(_.get('/todos/:id', (ctx, id) => {
    const auth = ctx.headers['x-token'];
    const { text, isDone } = db.get(id)

    const actions = []
    if( auth === 'admin') {
        actions.push({
            name: 'delete',
            title: 'Delete',
            method: 'DELETE',
            href: `todos/${id}`,
            type: 'application/json',
        })
    }

    if( auth === 'user' || auth === 'admin' ) {
        const finishAction = {
            name: 'finish',
            title: 'Finish',
            method: 'PATCH',
            href: `todos/${id}`,
            type: 'application/json',
            fields: [{
                name: 'isDone',
                type: 'boolean',
                value: true,
            }]
        }

        const unFinishAction = {
            name: 'unfinish',
            title: 'Unfinish',
            method: 'PATCH',
            href: `todos/${id}`,
            type: 'application/json',
            fields: [{
                name: 'isDone',
                type: 'boolean',
                value: false,
            }]
        }

        actions.push(isDone ? unFinishAction : finishAction)
    }
    
    const todo = siren({
        class: ['todo-item'],
        properties: {
            text,
            isDone,
        },
        actions,
    })

    ctx.body = todo
}))

app.use(_.get('/permissions', ctx => {
    ctx.body = ['user', 'admin']
}))

app.use(_.get('/', ctx => {
    ctx.host
    ctx.body = ctx.host
}))

app.listen(3000)